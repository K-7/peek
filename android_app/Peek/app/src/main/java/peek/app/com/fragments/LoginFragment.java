package peek.app.com.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toolbar;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import java.util.Arrays;

import peek.app.com.R;


public class LoginFragment extends Fragment {

    private UiLifecycleHelper mUiHelper;
    private Session.StatusCallback mCallback = new Session.StatusCallback()
    {
        @Override
        public void call(Session session, SessionState state, Exception exception)
        {
            onSessionStateChange(session, state, exception);
        }
    };

    private ProgressBar mProgressBar;
    private OnFragmentInteractionListener mListener;
    private boolean mMadeAsyncRequest = false;
    private Toolbar mToolbar;
    private LoginButton mAuthButton;


    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUiHelper = new UiLifecycleHelper(getActivity(), mCallback);
        mUiHelper.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        mAuthButton = (LoginButton) rootView.findViewById(R.id.authButton);
        mAuthButton.setFragment(this);
        mAuthButton.setReadPermissions(Arrays.asList("public_profile", "user_friends", "email"));
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.GONE);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        mUiHelper.onPause();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mUiHelper.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        mUiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        mUiHelper.onSaveInstanceState(outState);
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
//        public void onFragmentInteraction(Uri uri);
    }

    private void onSessionStateChange(final Session session, final SessionState state, Exception exception)
    {
        if (state.isOpened())
        {
//            if (!mPreferenceHelper.isLoggedIn() && !mMadeAsyncRequest)
//            {
//                mAuthButton.setVisibility(View.GONE);
//                mProgressBar.setVisibility(View.VISIBLE);
//                Bundle params = new Bundle();
//                params.putBoolean("redirect", false);
//                params.putString("type", "normal");
//                new Request(
//                        session,
//                        "/me/picture",
//                        params,
//                        HttpMethod.GET,
//                        new Request.Callback()
//                        {
//                            public void onCompleted(Response response)
//                            {
//                                try
//                                {
//                                    JSONObject responseJson = new JSONObject(response.getRawResponse());
//                                    JSONObject dataJson = responseJson.getJSONObject("data");
//                                    String userProfilePhotoUrl = dataJson.getString("url");
//                                    getUserData(session, userProfilePhotoUrl);
//                                } catch (JSONException e)
//                                {
//                                    e.printStackTrace();
//                                    Toast.makeText(getActivity(), getString(R.string.opps_something_went_wrong), Toast.LENGTH_SHORT).show();
//                                    onAuthFailed(getString(R.string.opps_something_went_wrong));
//                                } catch (Exception e)
//                                {
//                                    e.printStackTrace();
//                                    Toast.makeText(getActivity(), getString(R.string.opps_something_went_wrong), Toast.LENGTH_SHORT).show();
//                                    onAuthFailed(getString(R.string.opps_something_went_wrong));
//                                }
//                            }
//                        }
//                ).executeAsync();
//                mMadeAsyncRequest = true;
//            }
//        } else if (state.isClosed())
//        {
//            Log.i(TAG, "Logged out...");
//            mPreferenceHelper.setIsLogin(false);
        }
    }

}
