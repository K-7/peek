package peek.app.com.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import peek.app.com.R;
import peek.app.com.fragments.LoginFragment;

public class Login extends AppCompatActivity implements LoginFragment.OnFragmentInteractionListener {

    private LoginFragment mLoginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        if (savedInstanceState == null)
        {
            // Add the fragment on initial activity setup
            mLoginFragment = new LoginFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, mLoginFragment)
                    .commit();
        } else
        {
            // Or set the fragment from restored state info
            mLoginFragment = (LoginFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.container);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onAuthFailed(String message)
    {
        if (mLoginFragment != null && mLoginFragment.isAdded())
        {
//            mLoginFragment.onAuthFailed(message);
            Toast.makeText(getApplicationContext(), "Please enter the Username", Toast.LENGTH_SHORT).show();
            return;
        }
    }
}
